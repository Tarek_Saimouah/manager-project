const app = require('./app');
const connectDB = require('./api/database/db');

const PORT = process.env.PORT || 5000;

connectDB()
  .then(() => {
    console.log('Database connected successfully.');
    app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));
  })
  .catch((error) => {
    console.error('Process terminated! cannot connect to database.');
    console.error(error.message);
    process.exit(0);
  });
