const router = require('express').Router();
const verifyCleanBody = require('../middleware/cleanBody');
const managerSchemaValidation = require('../middleware/schemaValidation');
const verifyAuthorization = require('../middleware/apiAuthorization');

const { signUp, login } = require('../controllers/manager.controller');
const {
  createAccountLimiter,
  LoginLimiter,
} = require('../middleware/rateLimiter');

router
  .route('/sign-up')
  .post(
    verifyAuthorization,
    createAccountLimiter,
    verifyCleanBody,
    managerSchemaValidation,
    signUp
  );

router
  .route('/login')
  .post(
    verifyAuthorization,
    LoginLimiter,
    verifyCleanBody,
    managerSchemaValidation,
    login
  );

module.exports = router;
