const {
  getManagers,
  updateManager,
  deleteManager,
} = require('../controllers/admin.controller');
const adminAuth = require('../middleware/adminAuth');
const superAdminAuth = require('../middleware/superAdminAuth');
const verifyToken = require('../middleware/verifyToken');
const verifyAuthorization = require('../middleware/apiAuthorization');

const router = require('express').Router();

router
  .route('/managers')
  .get(verifyAuthorization, verifyToken, adminAuth, getManagers);

router
  .route('/managers/:userId')
  .patch(verifyAuthorization, verifyToken, superAdminAuth, updateManager)
  .delete(verifyAuthorization, verifyToken, adminAuth, deleteManager);

module.exports = router;
