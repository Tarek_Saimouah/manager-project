const Manager = require('../database/model/managerSchema');

const adminAuth = async (req, res, next) => {
  // check requester user role, just ['SuperAdmin', 'Admin'] can perform this operation
  const requesterUser = await Manager.findById(req.user.userId);

  if (!requesterUser) {
    return res.status(400).json({ message: 'Bad Request' });
  }

  if (requesterUser.role !== 'SuperAdmin' && requesterUser.role !== 'Admin') {
    return res.status(401).send('Unauthorized client!');
  }

  // requester user is authorized
  next();
};

module.exports = adminAuth;
