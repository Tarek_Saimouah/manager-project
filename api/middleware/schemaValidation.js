const joi = require('joi');

const managerSchema = joi.object({
  userName: joi.string().trim(true).required(),
  password: joi.string().min(6).trim(true).required(),
});

const managerSchemaValidation = (req, res, next) => {
  const payload = {
    userName: req.body.userName,
    password: req.body.password,
  };

  const { error } = managerSchema.validate(payload);

  if (error) {
    return res.status(406).json({
      error: `Error in user data: ${error.message}`,
    });
  } else next();
};

module.exports = managerSchemaValidation;
