const sanitize = require('mongo-sanitize');

const verifyCleanBody = (req, res, next) => {
  try {
    req.body = sanitize(req.body);
    next();
  } catch (error) {
    console.log('clean-body-error', error);
    return res.status(500).json({
      message: 'Could not sanitize body',
    });
  }
};

module.exports = verifyCleanBody;
