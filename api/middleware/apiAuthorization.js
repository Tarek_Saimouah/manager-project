const verifyAuthorization = (req, res, next) => {
  // check for 'Authorization-Key' header
  // 'Authorization-Key' header needed as a secretKey to make api private
  const authorizationKey = req.header('Authorization-Key');

  if (
    authorizationKey === undefined ||
    authorizationKey !== process.env.AUTHORIZATION_KEY
  ) {
    return res.status(401).json({ error: 'Unauthorized client!' });
  }

  next();
};

module.exports = verifyAuthorization;
