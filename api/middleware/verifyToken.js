const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
  if (
    req.headers['x-access-token'] &&
    req.headers['x-access-token'].startsWith('Bearer')
  ) {
    const token = req.headers['x-access-token'].split(' ')[1];

    try {
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      req.user = decoded;
    } catch (err) {
      return res.status(401).send('Invalid Token');
    }
    return next();
  }

  // token not provided
  return res.status(403).send('A token is required for authentication');
};

module.exports = verifyToken;
