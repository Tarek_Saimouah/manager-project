const mongoose = require('mongoose');

const ManagerSchema = mongoose.Schema({
  userName: {
    required: true,
    unique: true,
    type: String,
  },
  password: {
    required: true,
    type: String,
    minLength: 6,
    select: false,
  },
  role: {
    required: true,
    type: String,
    default: 'Basic',
  },
});

const Manager = mongoose.model('Manager', ManagerSchema);

module.exports = Manager;
