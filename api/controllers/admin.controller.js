const Manager = require('../database/model/managerSchema');

const getManagers = async (req, res) => {
  /*
   * GET
   *
   * BASE_URL/api/admin/managers
   *
   */

  try {
    const managers = await Manager.find();

    if (!managers) {
      return res.status(404).json({ message: 'Data not found' });
    }

    const response = {
      total_results: managers.length,
      results: managers,
    };

    return res.status(200).send(response);
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

const updateManager = async (req, res) => {
  /*
   * PATCH
   *
   * BASE_URL/api/admin/managers/:userId
   *
   * Update data in request body
   *
   */

  const userId = req.params.userId;
  const { userName, role } = req.body;

  if (!userName && !role) {
    return res.status(400).json({ message: 'Provide data for update!' });
  }

  let update = {};

  if (userName && role) {
    update = { $set: { userName, role } };
  } else if (userName && !role) {
    update = { $set: { userName } };
  } else {
    update = { $set: { role } };
  }

  await Manager.findByIdAndUpdate(userId, update)
    .then((updatedUser) => {
      if (!updatedUser) {
        return res.status(404).json({ message: 'User not found' });
      }

      res.status(201).json({
        message: `Successfully updated the following user: (${userName}).`,
      });
    })
    .catch((err) => {
      return res.status(500).json({ message: err.message });
    });
};

const deleteManager = async (req, res) => {
  /*
   * DELETE
   *
   * BASE_URL/api/admin/managers/:userId
   *
   */

  const userId = req.params.userId;

  try {
    const deletedUser = await Manager.findById(userId);

    if (!deletedUser) {
      return res.status(404).json({ message: 'User not found.' });
    }

    await deletedUser.remove();

    return res.status(200).json({
      message: `Successfully deleted the following user (${deletedUser.userName}).`,
    });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

module.exports = { getManagers, updateManager, deleteManager };
