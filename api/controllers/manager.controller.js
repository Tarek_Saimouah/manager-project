const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Manager = require('../database/model/managerSchema');

const signUp = async (req, res) => {
  /*
   * POST
   *
   * BASE_URL/api/manager/sign-up
   *
   * user in request body
   */

  const { userName, password, role } = req.body;

  try {
    const existedUser = await Manager.exists({ userName });

    if (existedUser) {
      return res.status(409).json({ message: 'User is already registered.' });
    }

    // create password hash
    const salt = await bcrypt.genSalt(Number(process.env.SALT_ROUNDS));
    const hash = await bcrypt.hash(password, salt);

    const user = await Manager.create({
      userName,
      password: hash,
      role,
    });

    if (user) {
      // user created successfully
      // create and send token
      const token = jwt.sign(
        { userId: user._id, userName },
        process.env.JWT_SECRET,
        {
          expiresIn: '1d',
        }
      );

      res.status(201).json({
        message: `user (${userName}) created successfully.`,
        token: token,
      });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

const login = async (req, res) => {
  /*
   * POST
   *
   * BASE_URL/api/manager/login
   *
   * user in request body
   */

  const { userName, password } = req.body;

  try {
    const user = await Manager.findOne({ userName }).select('+password');

    if (!user) {
      // user not found
      return res
        .status(404)
        .json({ message: 'Wrong email and password combinations' });
    }

    // user found, compare passwords
    const match = await bcrypt.compare(password, user.password);

    if (!match) {
      // wrong password
      return res
        .status(404)
        .json({ message: 'Wrong email and password combinations' });
    }

    // create and send token
    const token = jwt.sign(
      { userId: user._id, userName },
      process.env.JWT_SECRET,
      {
        expiresIn: '1d',
      }
    );

    res.status(200).json({
      message: `user (${userName}) login successfully.`,
      token: token,
    });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

module.exports = { signUp, login };
