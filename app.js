require('dotenv').config();
const express = require('express');
const managerRouter = require('./api/routes/manager.router');
const adminRouter = require('./api/routes/admin.router');
const { apiLimiter } = require('./api/middleware/rateLimiter');
const helmet = require('helmet');
const cors = require('cors');

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const CLIENT_URL = process.env.CLIENT_URL || '*';
app.use(
  cors({
    origin: CLIENT_URL,
  })
);

app.use(apiLimiter);
app.use(helmet());

app.use('/api/manager', managerRouter);
app.use('/api/admin', adminRouter);

module.exports = app;
